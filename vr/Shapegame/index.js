import React,{Component} from 'react';
import { AppRegistry, StyleSheet , asset , Pano , Text , View , Box,
         Cylinder , Sphere
       } from 'react-vr';

const shapes = [Cylinder,Box,Sphere];

export default class Shapegame extends Component {
    constructor(props){
      super(props);
    }

    _render(){
        let Component = shapes[this.props.shapeNum];
        return (
            <Component
                style={{
                    transform: this.props.transform,
                    color:'#fff'
                }}
            />
        );
    }

    render(){

      return (
        <View>
            <Text style={styles.text}>
                Find the Odd
            </Text>
            { this._render() }
        </View>
      )
    }
  }

const styles = StyleSheet.create({
    text:{
        transform : [
            { translate : [ -0.1 , 1.5 , -3 ] }
        ],
        backgroundColor:'#fff',
        color:'#000'
    },
    CustomComp:{
        backgroundColor:'#fff',
        transform:[
            {translate: [0,0,-5]}
        ]
    }
});