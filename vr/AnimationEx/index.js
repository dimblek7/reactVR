import React,{Component} from 'react';
import { asset , Animated } from 'react-vr';

export default class AnimationEx extends Component {

  constructor(){
    super();
    this.state = {
      zValue : new Animated.Value(-2)
    };
  }

  _boom(){
    Animated.spring(this.state.zValue,{toValue: -1.7}).start();
  }
  _bam(){
    Animated.spring(this.state.zValue,{toValue: -2}).start();
  }

  render () {
    return (
          <Animated.Image
            source={asset('EinsteinYoung.jpg')}
            onEnter={() => { this._boom() }}
            onExit={() => { this._bam() }}
            style={{
              transform:[
                {translateZ:this.state.zValue}
              ],
              height:0.5, width:0.5,
              layoutOrigin: [0.5, 0.5]
            }}
            >

          </Animated.Image>
     )
  }
}

