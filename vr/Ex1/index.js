import React,{Component} from 'react';
import { AppRegistry , asset , Cylinder,AmbientLight, Animated , Pano , Text , View , Box , Model } from 'react-vr';

export default class Ex1 extends Component {
  constructor(){
    super();
    this.state = {    };
  }

  render () {
    return (
      <View>
        <AmbientLight intensity={ 2.6 }  />
        
        <Model
          source={{obj:asset('earth.obj'), mtl:asset('earth.mtl')}}
          lit={true}
          style={{
            transform:[
              { translate: [-25,0,-70] },
              {scale: 0.05 }, 
              {rotateY: -130}, 
              {rotateX: 20}, 
              {rotateZ: -10}
            ]
          }}
        />
        
        <Model 
          style={{ 
            transform: [ 
              {translate: [10, 10, -100]}, 
              {scale: 0.05}, ] 
          }} 
          source={{obj:asset('moon.obj'), mtl:asset('moon.mtl')}} 
          lit={true}
        />

        <Model 
          style={{ 
            transform: [ 
              {translate: [30, 10, -80]}, 
              {scale: 0.05}, ] 
          }} 
          source={{obj:asset('Caribean_Princess.obj'), mtl:asset('Caribean_Princess.mtl')}} 
          lit={true}
        />
        
      </View>
     )
  }
}

