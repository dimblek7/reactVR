import React,{Component} from 'react';
import { Model , asset } from 'react-vr';


export default class CustomModels extends Component {
    constructor(props){
      super(props);
    }

    render(){

      return (
        <Model
            source={{
              obj: asset('Caribean_Princess.obj'),
              mtl: asset('Caribean_Princess.mtl')
            }}
            style={{
              transform:[
                {translate:[0,1,-50]},
              ]
            }}
          />
      )
    }
  }

