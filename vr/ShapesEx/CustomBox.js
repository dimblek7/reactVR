import React,{Component} from 'react';
import { AppRegistry , asset , Pano , Text , View , Box } from 'react-vr';

export default class CustomBox extends Component {
    constructor(props){
      super(props);
    }

    render(){
      return (
        <View>
          <Box
                dimWidth={0.8}
                dimHeight={0.8}
                dimDepth={0.8}
                wireframe={true}
                style={{
                    color: 'white',
                    transform: [
                      { translate: [0,0,0] },
                      { translateX: this.props.translateX },
                      { translateY: 0 },
                      { translateZ: -4 },
                      { rotateX: 0 },
                      { rotateY: this.props.rotateY },
                      { rotateZ: 0 }
                    ]
                  }
                }
              >
                <Text style={{color:'white',backgroundColor:'grey',fontSize:0.2,textAlign:'center'}}>
                    {this.props.name}
                </Text>
          </Box>

        </View>
      )
    }
  }