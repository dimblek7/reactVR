import React,{Component} from 'react';
import { AppRegistry ,Cylinder , asset , Pano , Text , View , Box } from 'react-vr';

class CustomCylinder extends Component {
    constructor(props){
        super(props);
      }
    render(){
        return(
            <Cylinder
                radiusBottom = {0.5}
                radiusTop = {0.5}
                dimHeight = {0.4}
                segments = {10}
                texture={'http://i.imgur.com/bEBgA.jpg'}
                style={{
                transform :[
                    {translate : [ -0, 1, -3 ]},
                    {rotateX : this.props.rotateX}
                ]
                }}
            >
                <Text style={{color:'white',backgroundColor:'grey',textAlign:'center'}}>
                    Neospheres
                </Text>
            </Cylinder>
        )
    }
}

export default CustomCylinder;