import React,{Component} from 'react';
import { AppRegistry , Plane,Sphere ,Cylinder , asset , Pano , Text , View , Box } from 'react-vr';
import CustomBox from './CustomBox';
import CustomCylinder from './CustomCylinder';
export default class Basics extends Component {
    constructor(){
      super();
      this.state = {
        rotateY : 0,
        rotateX : 0
      };

      setInterval( () => {
        if(this.state.rotateY < 350){
          this.setState({ rotateY : this.state.rotateY+2 , rotateX : this.state.rotateX+2 });
        }else {
          this.setState({ rotateY : 2 , rotateX :2 });
        }
      } , 20);
    }

    render () {
      return (
        <View>
            <CustomBox
              translateX = {-2}
              rotateY = { this.state.rotateY }
              name = {"Rahul"}
            />
            <CustomBox
              translateX = {0}
              rotateY = { this.state.rotateY }
              name = {"Narayan"}
            />
            <CustomBox
              translateX = {2}
              rotateY = { this.state.rotateY }
              name = {"Kaustubh"}
            />
            <CustomCylinder
               rotateX={this.state.rotateX}
            />
            <Sphere
              widthSegment={20}
              heightSegment={20}
              radius={0.4}
              texture={'http://i.imgur.com/bEBgB.jpg'}
              style={{
                color:'white',
                transform:[
                  { translate: [0.2,-1,-3] },
                  { rotateX : this.state.rotateX }
                ]
              }}
            />
            <Plane
              dimWidth={2}
              dimHeight={2}
              texture={'http://i.imgur.com/bEBgA.jpg'}
              style={{
                color:'white',
                transform:[
                  { translate: [0.2,-1,-3] },
                  { rotateX : -65 }
                ]
              }}
            />
        </View>
       )
    }
  }
